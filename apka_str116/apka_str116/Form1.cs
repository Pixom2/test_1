﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace apka_str116
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e) // generuje procedury obsługi zdarzenia
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            int y = 6;
            int result = 0;
            while (y > 3)
            {
                result += y;
                y -= 1;
            }
            myLabel.Text = result.ToString();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = "Przemo";
            int x = 5;
            double d = Math.PI / 2;
            if ((x > 3) && (name.Equals("Przemo"))) Console.WriteLine("spelnione");
            myLabel.Height = 170;
            if (checkBox1.Checked == true) myLabel.Text = "SPELNILO TWOJ WARUNEK";
            
            //myLabel.Text = "nazwa to: " + name + '\n' +" x jest równe: " + x + "\n d jest równe: " + d; // \n to sekwencja formatująca


        }

        private void button2_Click(object sender, EventArgs e)
        {
            int x = 5;
            if (x == 5)
                myLabel.Text += "równe";
            else
                myLabel.Text += "nie rowne";
        }

        private void myLabel_TextChanged(object sender, EventArgs e)
        {
            while (Visible)
            {
                for (int c = 0; c < 254 && Visible; c++)
                {
                    myLabel.BackColor = Color.FromArgb(c, 255 - c, c);
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(3);
                }
                for (int c = 254; c > 0 && Visible; c--)
                {
                    myLabel.BackColor = Color.FromArgb(c, 255 - c, c);
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(3);
                }
            }


        }
    }
}
