﻿namespace program_nawigacyjny_str147
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.thingToSay = new System.Windows.Forms.TextBox();
            this.numberOfTimes = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.talkToMe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfTimes)).BeginInit();
            this.SuspendLayout();
            // 
            // thingToSay
            // 
            this.thingToSay.Location = new System.Drawing.Point(126, 28);
            this.thingToSay.Name = "thingToSay";
            this.thingToSay.Size = new System.Drawing.Size(381, 20);
            this.thingToSay.TabIndex = 0;
            // 
            // numberOfTimes
            // 
            this.numberOfTimes.Location = new System.Drawing.Point(126, 68);
            this.numberOfTimes.Name = "numberOfTimes";
            this.numberOfTimes.Size = new System.Drawing.Size(120, 20);
            this.numberOfTimes.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Powiedz to:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // talkToMe
            // 
            this.talkToMe.Location = new System.Drawing.Point(109, 111);
            this.talkToMe.Name = "talkToMe";
            this.talkToMe.Size = new System.Drawing.Size(303, 23);
            this.talkToMe.TabIndex = 4;
            this.talkToMe.Text = "Mów do mnie !!!";
            this.talkToMe.UseVisualStyleBackColor = true;
            this.talkToMe.Click += new System.EventHandler(this.talkToMe_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 146);
            this.Controls.Add(this.talkToMe);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numberOfTimes);
            this.Controls.Add(this.thingToSay);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numberOfTimes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox thingToSay;
        private System.Windows.Forms.NumericUpDown numberOfTimes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button talkToMe;
    }
}

